package com.practice.agendalistview;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.SearchView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;

public class listActivity extends AppCompatActivity {
    private TableLayout tblLista;
    private ArrayList<Contacto> contactos;
    private ArrayList<Contacto> bufferList;
    private int originalIndex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        tblLista = (TableLayout) findViewById(R.id.tblLista);
        Bundle bundle = getIntent().getExtras();
        contactos = (ArrayList<Contacto>) bundle.getSerializable("contactos");
        bufferList = contactos;
        Button btnNuevo = (Button) findViewById(R.id.btnNuevo);
        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(Activity.RESULT_CANCELED);
                finish();
            }
        });
        cargarContactos();
    }

    public void cargarContactos(){
        for(int x = 0; x < contactos.size(); x++){
            final Contacto c = new Contacto(contactos.get(x));
            TableRow nRow = new TableRow(listActivity.this);
            TextView nText = new TextView(listActivity.this);
            nText.setText(c.getNombre());

            nText.setTextSize(TypedValue.COMPLEX_UNIT_PT,6);
            nText.setTextColor((c.isFavorito()) ? Color.BLUE : Color.BLACK);
            nRow.addView(nText);

            Button nButton = new Button(listActivity.this);
            nButton.setText(R.string.accVer);
            nButton.setTextSize(TypedValue.COMPLEX_UNIT_PT,6);
            nButton.setTextColor(Color.BLACK);

            nButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    for(int i=0;i<bufferList.size();i++){
                        if(c.getNombre().equals(bufferList.get(i).getNombre()) &&
                                c.getTelefono1().equals(bufferList.get(i).getTelefono1())){
                            Contacto c = (Contacto) v.getTag(R.string.contacto_g);
                            Intent intent = new Intent();
                            Bundle bundle = new Bundle();
                            bundle.putSerializable("contacto", c);
                            //bundle.putInt("index",(int) v.getTag(R.string.contacto_g_index));
                            bundle.putInt("index",i);
                            intent.putExtras(bundle);
                            setResult(RESULT_OK,intent);
                            finish();
                        }
                    }
                }
            });
            nButton.setTag(R.string.contacto_g, c);
            nButton.setTag(R.string.contacto_g_index,x);
            nRow.addView(nButton);
            tblLista.addView(nRow);
        }
    }

    private void searchOnTable(String queryText){
        ArrayList<Contacto> filterList = new ArrayList<>();
        String query = queryText.toUpperCase();
        for (int i=0;i<bufferList.size();i++){
            if(bufferList.get(i).getNombre().toUpperCase().contains(query)){
                filterList.add(bufferList.get(i));
            }
        }
        contactos = filterList;
        tblLista.removeAllViews();
        cargarContactos();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.list_menu,menu);
        MenuItem menuItem = menu.findItem(R.id.txtBuscar);
        SearchView searchView = (SearchView) menuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                searchOnTable(newText);
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

}
