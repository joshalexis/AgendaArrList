package com.practice.agendalistview;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private final ArrayList<Contacto> contactos = new ArrayList<>();
    private EditText edtNombre;
    private EditText edtDireccion;
    private EditText edtTelefono;
    private EditText edtTelefono2;
    private EditText edtNotas;
    private CheckBox cbxFavorito;
    private Contacto saveContact;
    private int savedIndex;
    private int index;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        edtNombre = (EditText) findViewById(R.id.txtNombre);
        edtDireccion = (EditText) findViewById(R.id.txtDireccion);
        edtTelefono = (EditText) findViewById(R.id.txtTel1);
        edtTelefono2 = (EditText) findViewById(R.id.txtTel2);
        edtNotas = (EditText) findViewById(R.id.txtNota);
        cbxFavorito = (CheckBox) findViewById(R.id.chkFavorito);
        Button btnGuardar = (Button) findViewById(R.id.btnGuardar);
        Button btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        Button btnListar = (Button) findViewById(R.id.btnListar);
        Button btnSalir = (Button) findViewById(R.id.btnSalir);

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edtNombre.getText().toString().equals("") || edtDireccion.getText().toString().equals("")
                        || edtTelefono.getText().toString().equals("")){
                    Toast.makeText(MainActivity.this, R.string.msgError, Toast.LENGTH_SHORT).show();
                }else{
                    Contacto nContacto = new Contacto();
                    index = contactos.size();
                    if(saveContact != null){
                        contactos.remove(savedIndex);
                        nContacto = saveContact;
                        index = savedIndex;
                    }
                    nContacto.setNombre(edtNombre.getText().toString());
                    nContacto.setTelefono1(edtTelefono.getText().toString());
                    nContacto.setTelefono2(edtTelefono2.getText().toString());
                    nContacto.setDomicilio(edtDireccion.getText().toString());
                    nContacto.setNotas(edtNotas.getText().toString());
                    nContacto.setFavorito(cbxFavorito.isChecked());
                    contactos.add(index,nContacto);
                    Toast.makeText(MainActivity.this, R.string.mensaje,Toast.LENGTH_SHORT).show();
                    saveContact = null;
                    limpiar();
                }
            }
        });


        btnListar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this,listActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("contactos",contactos);
                i.putExtras(bundle);
                startActivityForResult(i,0);
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent){
        super.onActivityResult(requestCode,resultCode,intent);
        if(intent != null){
            Bundle bundle = intent.getExtras();

            saveContact = (Contacto) bundle.getSerializable("contacto");
            savedIndex = bundle.getInt("index");
            edtNombre.setText(saveContact.getNombre());
            edtTelefono.setText(saveContact.getTelefono1());
            edtTelefono2.setText(saveContact.getTelefono2());
            edtDireccion.setText(saveContact.getDomicilio());
            edtNotas.setText(saveContact.getNotas());
            cbxFavorito.setChecked(saveContact.isFavorito());
        }else{
            limpiar();
        }
    }

    public void limpiar(){
        saveContact = null;
        edtDireccion.setText("");
        edtNombre.setText("");
        edtNotas.setText("");
        edtTelefono.setText("");
        edtTelefono2.setText("");
        cbxFavorito.setChecked(false);
    }
}
